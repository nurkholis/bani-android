import 'dart:async';

import 'package:bani/screen/dashboard/dashboard_screen.dart';
import 'package:flutter/material.dart';
import 'config/palette.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Abadi',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primarySwatch: Pallet.primaryColor,
      ),
      home: DashboardScreen(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:bani/config/palette.dart';

enum SnackBarType { succeess, alert }

class Dialogs {
  Future<void> showLoadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: SimpleDialog(
            children: <Widget>[
              Center(
                child: Column(
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Loading...",
                      style: Style.fontMedium,
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future<void> showSnackbar({
    @required GlobalKey<ScaffoldState> key,
    @required String title,
    @required String subtitle,
    @required SnackBarType snackBarType,
  }) async {
    key.currentState.showSnackBar(
      SnackBar(
        content: Container(
          padding: EdgeInsets.all(5.0),
          child: Wrap(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    snackBarType == SnackBarType.succeess
                        ? Icons.check_circle_outline
                        : Icons.info_outline,
                    color: Colors.white,
                    size: 30.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: Pallet.fontMediumSize,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(subtitle),
              ),
            ],
          ),
        ),
        duration: Duration(seconds: 3),
        backgroundColor:
            snackBarType == SnackBarType.succeess ? Colors.green : Colors.red,
      ),
    );
  }

  Future<void> showConfirmDialog(
      BuildContext context, String subtitle, void action()) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Row(children: [
            Icon(
              Icons.info_outline,
              color: Colors.orangeAccent,
            ),
            Text(
              ' Konfirmasi',
              style: TextStyle(color: Colors.black54),
            )
          ]),
          content: Text(
            subtitle,
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
          actions: [
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              elevation: 0.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Colors.grey),
              ),
              // color: Colors.grey,
              child: Text(
                'Batal',
                style: Style.fontMedium,
              ),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
                action();
              },
              elevation: 0.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Pallet.primaryColor),
              ),
              child: Text(
                'Ok, Lanjut',
                style: Style.fontMedium,
              ),
            ),
          ],
        );
      },
    );
  }
}

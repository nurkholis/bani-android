class EndPoint {
  // static final domain = '192.168.43.60:8000';
  static final domain = 'bani.kodejariah.com';
  static final path = {
    'album': '/api/album',
    'anggota': '/api/anggota',
    'berita': '/api/berita',
    'foto': '/api/foto',
    'laporan': '/api/laporan',
  };
}

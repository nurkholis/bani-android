import 'package:flutter/material.dart';

class Pallet {
  static const Color primaryColor = MaterialColor(
    0xFF066699,
    {
      50: Color.fromRGBO(6, 102, 153, .1),
      100: Color.fromRGBO(6, 102, 153, .2),
      200: Color.fromRGBO(6, 102, 153, .3),
      300: Color.fromRGBO(6, 102, 153, .4),
      400: Color.fromRGBO(6, 102, 153, .5),
      500: Color.fromRGBO(6, 102, 153, .6),
      600: Color.fromRGBO(6, 102, 153, .7),
      700: Color.fromRGBO(6, 102, 153, .8),
      800: Color.fromRGBO(6, 102, 153, .9),
      900: Color.fromRGBO(6, 102, 153, 1),
    },
  );
  static const Color secondaryColor = Color(0xFFFFCD60);
  static const Color shadeColor = Color(0xFFEEEEF8);
  static const padding = 25.0;
  static const borderRadius = 20.0;
  static const fontSmallSize = 12.0;
  static const fontMediumSize = 15.0;
  static const fontLargeSize = 20.0;
}

class Style {
  static TextStyle fontSmall = TextStyle(
    fontSize: Pallet.fontSmallSize,
    color: Colors.black54,
  );
  static TextStyle fontSmallPrimary = TextStyle(
    fontSize: Pallet.fontSmallSize,
    color: Pallet.primaryColor,
  );
  static TextStyle fontMedium = TextStyle(
    fontSize: Pallet.fontMediumSize,
    color: Colors.black54,
  );
  static TextStyle fontMediumPrimary = TextStyle(
    fontSize: Pallet.fontMediumSize,
    color: Pallet.primaryColor,
  );
  static TextStyle fontLarge = TextStyle(
    fontSize: Pallet.fontLargeSize,
    color: Colors.black54,
  );
}

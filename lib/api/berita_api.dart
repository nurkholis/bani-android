import 'dart:convert';
import 'package:bani/config/end_point.dart';
import 'package:bani/model/berita_model.dart';
import 'package:http/http.dart' as http;

class BeritaApi {
  Future<BeritaModel> getBerita(Object param) async {
    try {
      final uri =
          Uri.http(EndPoint.domain, EndPoint.path['berita'] + '/1', param);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        return BeritaModel.fromJson(
          json.decode(response.body),
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}

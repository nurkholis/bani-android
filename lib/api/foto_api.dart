import 'dart:convert';
import 'package:bani/config/end_point.dart';
import 'package:bani/model/foto_model.dart';
import 'package:http/http.dart' as http;

class FotoApi {
  Future<FotoModel> getFoto(Object param) async {
    try {
      final uri =
          Uri.http(EndPoint.domain, EndPoint.path['foto'] + '/1', param);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        return FotoModel.fromJson(
          json.decode(response.body),
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}

import 'dart:convert';
import 'package:bani/config/end_point.dart';
import 'package:bani/model/album_model.dart';
import 'package:http/http.dart' as http;

class AlbumApi {
  Future<AlbumModel> getAlbum(Object param) async {
    try {
      final uri =
          Uri.http(EndPoint.domain, EndPoint.path['album'] + '/1', param);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        return AlbumModel.fromJson(
          json.decode(response.body),
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}

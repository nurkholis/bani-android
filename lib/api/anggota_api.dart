import 'dart:convert';
import 'package:bani/config/end_point.dart';
import 'package:bani/model/anggota_model.dart';
import 'package:http/http.dart' as http;

class AnggotaApi {
  Future<AnggotaModel> getAnggota(Object param) async {
    try {
      final uri =
          Uri.http(EndPoint.domain, EndPoint.path['anggota'] + '/1', param);
      final response = await http.get(uri);
      print(uri);
      if (response.statusCode == 200) {
        return AnggotaModel.fromJson(
          json.decode(response.body),
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}

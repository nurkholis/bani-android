import 'dart:convert';
import 'package:bani/config/end_point.dart';
import 'package:bani/model/gerai_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class GeraiApi {
  Future<GeraiModel> getGerai(Object param) async {
    try {
      final uri = Uri.http(EndPoint.domain, EndPoint.path['gerai'], param);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        return GeraiModel.fromJson(
          json.decode(response.body),
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<GeraiDataModel> getGeraiById(String id_gerai) async {
    try {
      final uri =
          Uri.http(EndPoint.domain, "${EndPoint.path['gerai']}/${id_gerai}");
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        return GeraiDataModel.fromJson(
          json.decode(response.body)["data"],
        );
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<bool> loginGerai(Object body) async {
    try {
      final uri = Uri.http(EndPoint.domain, EndPoint.path['gerai.login']);
      final response = await http.post(uri, body: body);
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        GeraiDataModel geraiDataModel = GeraiDataModel.fromJson(result["data"]);
        print(geraiDataModel.nama_gerai);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', result['meta']['token'].toString());
        prefs.setString('nama_gerai', geraiDataModel.nama_gerai);
        prefs.setString('alamat_gerai', geraiDataModel.alamat_gerai);
        prefs.setString('id_gerai', geraiDataModel.id_gerai);
        prefs.setString('nama_pemilik', geraiDataModel.nama_pemilik);
        prefs.setString('foto_gerai', geraiDataModel.foto_gerai);
        prefs.setString('email', geraiDataModel.email);
        return true;
      }
      return false;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  Future<String> resetPassword(String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final uri = Uri.http(
        EndPoint.domain,
        EndPoint.path['gerai'] + "/" + prefs.getString("id_gerai"),
      );
      final response = await http.post(uri, body: {
        "password": password,
        "_method": "put",
      }, headers: {
        'Authorization': 'Bearer ' + prefs.getString("token"),
        "Accept": "application/json",
      });
      print(response.body);
      if (response.statusCode == 200) {
        return "ok";
      }
      return "fail";
    } on Exception catch (e) {
      debugPrint(e.toString());
      return "fail";
    }
  }

  Future<String> postGerai(GeraiDataModel geraiDataModel) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String path = geraiDataModel.id_gerai == null
          ? EndPoint.path['gerai']
          : EndPoint.path['gerai'] + '/${geraiDataModel.id_gerai}';
      final uri = Uri.http(EndPoint.domain, path);
      final response = await http.post(uri, body: {
        "id_jenis_gerai": geraiDataModel.id_jenis_gerai.toString(),
        "id_gerai": geraiDataModel.id_gerai.toString(),
        "nama_gerai": geraiDataModel.nama_gerai.toString(),
        "alamat_gerai": geraiDataModel.alamat_gerai.toString(),
        "latitude": geraiDataModel.latitude.toString(),
        "longitude": geraiDataModel.longitude.toString(),
        "nama_pemilik": geraiDataModel.nama_pemilik.toString(),
        "foto_gerai": geraiDataModel.foto_gerai.toString(),
        "email": geraiDataModel.email.toString(),
        "password": geraiDataModel.password.toString(),
        "_method": geraiDataModel.id_gerai == null ? "post" : "put",
      }, headers: {
        'Authorization': 'Bearer ' + prefs.getString("token"),
        "Accept": "application/json",
      });
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        GeraiDataModel geraiDataModel = GeraiDataModel.fromJson(result["data"]);
        print(geraiDataModel.nama_gerai);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', result['meta']['token'].toString());
        prefs.setString('nama_gerai', geraiDataModel.nama_gerai);
        prefs.setString('alamat_gerai', geraiDataModel.alamat_gerai);
        prefs.setString('id_gerai', geraiDataModel.id_gerai);
        prefs.setString('nama_pemilik', geraiDataModel.nama_pemilik);
        prefs.setString('foto_gerai', geraiDataModel.foto_gerai);
        prefs.setString('email', geraiDataModel.email);
        return "ok";
      }
      return response.body;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return e.toString();
    }
  }

  Future<String> gantiPassword(GeraiDataModel geraiDataModel) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final uri = Uri.http(
        EndPoint.domain,
        EndPoint.path['gerai'] + "/" + prefs.getString("id_gerai"),
      );
      final response = await http.post(uri, body: {
        "password": geraiDataModel.password,
        "foto_gerai": "null",
        "_method": "put",
      }, headers: {
        'Authorization': 'Bearer ' + prefs.getString("token"),
        "Accept": "application/json",
      });

      if (response.statusCode == 200) {
        return "ok";
      }
      return response.body;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return "fail";
    }
  }

  Future<Map> getGeraiMeta(String id_gerai) async {
    try {
      final uri = Uri.http(
          EndPoint.domain, "${EndPoint.path['gerai.meta']}/${id_gerai}");
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        return data;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}

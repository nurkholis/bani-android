import 'package:bani/config/palette.dart';
import 'package:bani/model/berita_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html_view/flutter_html_view.dart';

class IsiBeritaScreen extends StatefulWidget {
  BeritaDataModel beritaDataModel;
  IsiBeritaScreen({
    @required this.beritaDataModel,
  });
  @override
  _IsiBeritaScreenState createState() => _IsiBeritaScreenState();
}

class _IsiBeritaScreenState extends State<IsiBeritaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/preload_image.png",
                      image: widget.beritaDataModel.cover_berita,
                      fit: BoxFit.cover,
                      height: double.infinity,
                      width: double.infinity,
                      alignment: Alignment.center,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: Pallet.padding,
                      top: Pallet.padding * 2,
                    ),
                    child: SizedBox(
                      height: 50,
                      width: 50,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        elevation: 2,
                        color: Pallet.primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: Pallet.padding / 2,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Pallet.padding / 2),
              child: Text(
                widget.beritaDataModel.judul_berita,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(Pallet.padding / 2),
              child: Text(
                "Oleh " + widget.beritaDataModel.admin.nama_admin,
                style: Style.fontSmall,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Pallet.padding / 2),
              child: HtmlView(
                data: widget.beritaDataModel.isi_berita,
                scrollable: false,
              ),
              // child: Text(
              //   widget.beritaDataModel.isi_berita,
              //   style: Style.fontMedium,
              // ),
            ),
          ],
        ),
      ),
    );
  }
}

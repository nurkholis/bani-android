import 'package:bani/api/berita_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/berita_model.dart';
import 'package:bani/screen/berita/isi_berita_screen.dart';
import 'package:flutter/material.dart';

class BeritaScreen extends StatefulWidget {
  @override
  _BeritaScreenState createState() => _BeritaScreenState();
}

class _BeritaScreenState extends State<BeritaScreen> {
  GlobalKey<ScaffoldState> scafold_key = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  BeritaModel beritaModel = BeritaModel();
  Future<BeritaModel> future;

  bool loadNext = false;
  bool loadData = false;

  Future<BeritaModel> getDataBerita({
    String search,
    String page,
    String limit,
  }) {
    return BeritaApi().getBerita({
      "search": search,
      "page": page == null ? "1" : page,
      "limit": limit == null ? "10" : limit,
    });
  }

  @override
  void initState() {
    future = getDataBerita();
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
        } else {
          if (beritaModel.pagination.current_page <
              beritaModel.pagination.total_pages) {
            setState(() {
              loadNext = true;
            });
            getDataBerita(
              page: (beritaModel.pagination.current_page + 1).toString(),
            ).then((BeritaModel r) {
              setState(() {
                loadNext = false;
                beritaModel.pagination = r.pagination;
                beritaModel.data.addAll(r.data);
              });
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafold_key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Berita",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            beritaModel = snapshot.data;
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  future = getDataBerita();
                });
              },
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                padding: EdgeInsets.only(top: Pallet.padding / 2),
                itemCount: beritaModel.data.length,
                itemBuilder: (BuildContext context, int i) {
                  if (loadNext && i == beritaModel.data.length - 1) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => IsiBeritaScreen(
                              beritaDataModel: beritaModel.data[i],
                            ),
                          ),
                        );
                      },
                      contentPadding: EdgeInsets.all(Pallet.padding / 2),
                      leading: Container(
                        width: 60.0,
                        height: 60.0,
                        child: ClipRRect(
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/images/preload_image.png",
                            image: beritaModel.data[i].cover_berita,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      title: Text(
                        beritaModel.data[i].judul_berita,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      subtitle: Text(
                        "Oleh " + beritaModel.data[i].admin.nama_admin,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black54,
                        ),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}

import 'package:bani/api/foto_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/album_model.dart';
import 'package:bani/model/foto_model.dart';
import 'package:bani/widget/photo_viewer_widget.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FotoScreen extends StatefulWidget {
  AlbumDataModel albumDataModel;
  FotoScreen({
    @required this.albumDataModel,
  });
  @override
  _FotoScreenState createState() => _FotoScreenState();
}

class _FotoScreenState extends State<FotoScreen> {
  GlobalKey<ScaffoldState> scafold_key = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  FotoModel fotoModel = FotoModel();
  Future<FotoModel> future;

  bool loadNext = false;
  bool loadData = false;

  Future<FotoModel> getDataFoto({
    String search,
    String page,
    String limit,
  }) {
    return FotoApi().getFoto({
      "search": search,
      "page": page == null ? "1" : page,
      "limit": limit == null ? "10" : limit,
      "id_album": widget.albumDataModel.id_album,
    });
  }

  @override
  void initState() {
    future = getDataFoto();
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
        } else {
          if (fotoModel.pagination.current_page <
              fotoModel.pagination.total_pages) {
            setState(() {
              loadNext = true;
            });
            getDataFoto(
              page: (fotoModel.pagination.current_page + 1).toString(),
            ).then((FotoModel r) {
              setState(() {
                loadNext = false;
                fotoModel.pagination = r.pagination;
                fotoModel.data.addAll(r.data);
              });
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafold_key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.albumDataModel.nama_album,
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            fotoModel = snapshot.data;
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  future = getDataFoto();
                });
              },
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                padding: EdgeInsets.only(top: Pallet.padding / 2),
                itemCount: fotoModel.data.length,
                itemBuilder: (BuildContext context, int i) {
                  if (loadNext && i == fotoModel.data.length - 1) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return Container(
                      height: 200,
                      width: double.infinity,
                      padding: EdgeInsets.all(Pallet.padding / 2),
                      child: Hero(
                        tag: fotoModel.data[i].foto,
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => PhotoViewerWidget(
                                    netwokImage: fotoModel.data[i].foto),
                              ),
                            );
                          },
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/images/preload_image.png",
                            image: fotoModel.data[i].foto,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    );
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}

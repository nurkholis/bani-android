import 'package:bani/api/laporan_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/laporan_model.dart';
import 'package:bani/screen/foto/foto_screen.dart';
import 'package:bani/widget/photo_viewer_widget.dart';
import 'package:flutter/material.dart';

class LaporanScreen extends StatefulWidget {
  @override
  _LaporanScreenState createState() => _LaporanScreenState();
}

class _LaporanScreenState extends State<LaporanScreen> {
  GlobalKey<ScaffoldState> scafold_key = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  LaporanModel laporanModel = LaporanModel();
  Future<LaporanModel> future;

  bool loadNext = false;
  bool loadData = false;

  Future<LaporanModel> getDataLaporan({
    String search,
    String page,
    String limit,
  }) {
    return LaporanApi().getLaporan({
      "search": search,
      "page": page == null ? "1" : page,
      "limit": limit == null ? "10" : limit,
    });
  }

  @override
  void initState() {
    future = getDataLaporan();
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
        } else {
          if (laporanModel.pagination.current_page <
              laporanModel.pagination.total_pages) {
            setState(() {
              loadNext = true;
            });
            getDataLaporan(
              page: (laporanModel.pagination.current_page + 1).toString(),
            ).then((LaporanModel r) {
              setState(() {
                loadNext = false;
                laporanModel.pagination = r.pagination;
                laporanModel.data.addAll(r.data);
              });
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafold_key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Laporan",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            laporanModel = snapshot.data;
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  future = getDataLaporan();
                });
              },
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                padding: EdgeInsets.only(top: Pallet.padding / 2),
                itemCount: laporanModel.data.length,
                itemBuilder: (BuildContext context, int i) {
                  if (loadNext && i == laporanModel.data.length - 1) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => PhotoViewerWidget(
                              netwokImage: laporanModel.data[i].laporan,
                            ),
                          ),
                        );
                      },
                      contentPadding: EdgeInsets.all(Pallet.padding / 2),
                      leading: Container(
                        width: 60.0,
                        height: 60.0,
                        child: ClipRRect(
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/images/preload_image.png",
                            image: laporanModel.data[i].laporan,
                            fit: BoxFit.cover,
                            height: double.infinity,
                            width: double.infinity,
                            alignment: Alignment.center,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      title: Text(
                        laporanModel.data[i].keterangan_laporan,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}

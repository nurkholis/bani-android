import 'package:bani/config/end_point.dart';
import 'package:bani/model/anggota_model.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:webview_flutter/webview_flutter.dart';

class BaganScreen extends StatefulWidget {
  AnggotaDataModel anggotaDataModel;
  BaganScreen({
    @required this.anggotaDataModel,
  });
  @override
  _BaganScreenState createState() => _BaganScreenState();
}

class _BaganScreenState extends State<BaganScreen> {
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    String url =
        "https://${EndPoint.domain}/bagan/${widget.anggotaDataModel.id_anggota}";
    print(url);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.anggotaDataModel.nama,
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Stack(
        children: <Widget>[
          WebView(
            initialUrl: url,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
              setState(() {
                isLoading = false;
              });
            },
          ),
          isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Stack(),
        ],
      ),
    );
  }
}

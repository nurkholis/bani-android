import 'package:bani/api/anggota_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/anggota_model.dart';
import 'package:bani/screen/anggota/bagan_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnggotaScreen extends StatefulWidget {
  @override
  _AnggotaScreenState createState() => _AnggotaScreenState();
}

class _AnggotaScreenState extends State<AnggotaScreen> {
  GlobalKey<ScaffoldState> scafold_key = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  AnggotaModel anggotaModel = AnggotaModel();
  Future<AnggotaModel> future;

  bool loadNext = false;
  bool loadData = false;
  TextEditingController cari = TextEditingController();
  FocusNode focusNode = FocusNode();

  Future<AnggotaModel> getDataAnggota({
    String search,
    String page,
    String limit,
  }) {
    return AnggotaApi().getAnggota({
      "search": search,
      "page": page == null ? "1" : page,
      "limit": limit == null ? "10" : limit,
    });
  }

  @override
  void initState() {
    future = getDataAnggota();
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
        } else {
          if (anggotaModel.pagination.current_page <
              anggotaModel.pagination.total_pages) {
            setState(() {
              loadNext = true;
            });
            getDataAnggota(
              page: (anggotaModel.pagination.current_page + 1).toString(),
            ).then((AnggotaModel r) {
              setState(() {
                loadNext = false;
                anggotaModel.pagination = r.pagination;
                anggotaModel.data.addAll(r.data);
              });
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafold_key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
        title: Container(
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.circular(
              Pallet.borderRadius,
            ),
          ),
          child: SizedBox(
            height: 35,
            child: TextFormField(
              controller: cari,
              focusNode: focusNode,
              onEditingComplete: () {
                FocusScope.of(context).unfocus();
                setState(() {
                  future = getDataAnggota(search: cari.text);
                });
              },
              onChanged: (String text) async {
                if (text == "") {
                  setState(() {
                    future = getDataAnggota();
                  });
                }
              },
              textInputAction: TextInputAction.search,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.black54,
                  size: 16,
                ),
                hintText: "Mau kepoin siapa?",
                border: InputBorder.none,
              ),
            ),
          ),
        ),
      ),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            anggotaModel = snapshot.data;
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  future = getDataAnggota();
                });
              },
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                padding: EdgeInsets.only(top: Pallet.padding / 2),
                itemCount: anggotaModel.data.length,
                itemBuilder: (BuildContext context, int i) {
                  if (loadNext && i == anggotaModel.data.length - 1) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => BaganScreen(
                              anggotaDataModel: anggotaModel.data[i],
                            ),
                          ),
                        );
                      },
                      contentPadding: EdgeInsets.all(Pallet.padding / 2),
                      leading: Container(
                        width: 60.0,
                        height: 60.0,
                        child: ClipRRect(
                          child: SvgPicture.network(
                            anggotaModel.data[i].foto,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      title: Text(
                        anggotaModel.data[i].almarhum == "1"
                            ? "(Alm) ${anggotaModel.data[i].nama}"
                            : "${anggotaModel.data[i].nama}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      subtitle:
                          Text("Generasi ke ${anggotaModel.data[i].generasi}"),
                    );
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}

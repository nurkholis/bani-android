import 'package:bani/api/album_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/album_model.dart';
import 'package:bani/screen/foto/foto_screen.dart';
import 'package:flutter/material.dart';

class AlbumScreen extends StatefulWidget {
  @override
  _AlbumScreenState createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {
  GlobalKey<ScaffoldState> scafold_key = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  AlbumModel albumModel = AlbumModel();
  Future<AlbumModel> future;

  bool loadNext = false;
  bool loadData = false;

  Future<AlbumModel> getDataAlbum({
    String search,
    String page,
    String limit,
  }) {
    return AlbumApi().getAlbum({
      "search": search,
      "page": page == null ? "1" : page,
      "limit": limit == null ? "10" : limit,
    });
  }

  @override
  void initState() {
    future = getDataAlbum();
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
        } else {
          if (albumModel.pagination.current_page <
              albumModel.pagination.total_pages) {
            setState(() {
              loadNext = true;
            });
            getDataAlbum(
              page: (albumModel.pagination.current_page + 1).toString(),
            ).then((AlbumModel r) {
              setState(() {
                loadNext = false;
                albumModel.pagination = r.pagination;
                albumModel.data.addAll(r.data);
              });
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafold_key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Album",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            albumModel = snapshot.data;
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  future = getDataAlbum();
                });
              },
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                padding: EdgeInsets.only(top: Pallet.padding / 2),
                itemCount: albumModel.data.length,
                itemBuilder: (BuildContext context, int i) {
                  if (loadNext && i == albumModel.data.length - 1) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => FotoScreen(
                              albumDataModel: albumModel.data[i],
                            ),
                          ),
                        );
                      },
                      contentPadding: EdgeInsets.all(Pallet.padding / 2),
                      leading: Container(
                        width: 60.0,
                        height: 60.0,
                        child: ClipRRect(
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/images/preload_image.png",
                            image: albumModel.data[i].cover_album,
                            fit: BoxFit.cover,
                            height: double.infinity,
                            width: double.infinity,
                            alignment: Alignment.center,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      title: Text(
                        albumModel.data[i].nama_album,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}

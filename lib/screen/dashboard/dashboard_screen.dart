import 'package:bani/api/berita_api.dart';
import 'package:bani/api/foto_api.dart';
import 'package:bani/config/palette.dart';
import 'package:bani/model/berita_model.dart';
import 'package:bani/model/foto_model.dart';
import 'package:bani/screen/album/album_screen.dart';
import 'package:bani/screen/anggota/anggota_screen.dart';
import 'package:bani/screen/berita/berita_screen.dart';
import 'package:bani/screen/dashboard/widget/berita_widget.dart';
import 'package:bani/screen/dashboard/widget/card_menu_widget.dart';
import 'package:bani/screen/dashboard/widget/carousel.dart';
import 'package:bani/screen/dashboard/widget/foto_widget.dart';
import 'package:bani/screen/laporan/laporan_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  Future<BeritaModel> futureBeritaModel;
  Future<FotoModel> futureFotoModel;
  @override
  void initState() {
    futureBeritaModel = BeritaApi().getBerita(
      {
        "limit": "10",
        "page": "1",
      },
    );
    futureFotoModel = FotoApi().getFoto(
      {
        "limit": "10",
        "page": "1",
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Padding(
          padding: EdgeInsets.symmetric(vertical: 0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(
                Pallet.borderRadius,
              ),
            ),
            child: SizedBox(
              height: 35,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => AnggotaScreen(),
                    ),
                  );
                },
                child: TextFormField(
                  enabled: false,
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.black54,
                  ),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black54,
                      size: 16,
                    ),
                    hintText: "Mau kepoin siapa?",
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: SizedBox(
              width: 40,
              height: 40,
              child: RaisedButton(
                padding: EdgeInsets.all(0),
                elevation: 0,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => AnggotaScreen(),
                    ),
                  );
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: Pallet.secondaryColor,
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            futureBeritaModel = BeritaApi().getBerita(
              {
                "limit": "10",
                "page": "1",
              },
            );
            futureFotoModel = FotoApi().getFoto(
              {
                "limit": "10",
                "page": "1",
              },
            );
          });
        },
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: Colors.white,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: Pallet.padding / 2,
                    ),
                    Carousel(),
                    SizedBox(
                      height: Pallet.padding / 2,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Menu",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Agak mirip pintu kemana saja Doraemon, temukan jalan pintas disini.",
                      ),
                    ),
                    SizedBox(
                      height: Pallet.padding / 2,
                    ),
                    Container(
                      child: GridView.count(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        crossAxisCount: 2,
                        padding: EdgeInsets.symmetric(
                          horizontal: Pallet.padding,
                        ),
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        children: <Widget>[
                          CardMenuWidget(
                            svgImage: "assets/images/icon/anggota.svg",
                            title: "Anggota",
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => AnggotaScreen(),
                                ),
                              );
                            },
                          ),
                          CardMenuWidget(
                            svgImage: "assets/images/icon/laporan.svg",
                            title: "Laporan",
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => LaporanScreen(),
                                ),
                              );
                            },
                          ),
                          CardMenuWidget(
                            svgImage: "assets/images/icon/berita.svg",
                            title: "Berita",
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => BeritaScreen(),
                                ),
                              );
                            },
                          ),
                          CardMenuWidget(
                            svgImage: "assets/images/icon/album.svg",
                            title: "Album",
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => AlbumScreen(),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: Pallet.padding,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: Pallet.padding / 2,
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: Pallet.padding),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Berita",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Update seputar informasi bani akan kami laporkan disini.",
                      ),
                    ),
                    SizedBox(
                      height: Pallet.padding / 2,
                    ),
                    BeritaWidget(futureBeritaModel: futureBeritaModel),
                    SizedBox(
                      height: Pallet.padding,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Foto Album",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
                      child: Text(
                        "Lihat foto-foto pertemuan bani tiap tahunnya disini.",
                      ),
                    ),
                    SizedBox(
                      height: Pallet.padding / 2,
                    ),
                    FotoWidget(futureFotoModel: futureFotoModel),
                    SizedBox(
                      height: Pallet.padding * 3,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

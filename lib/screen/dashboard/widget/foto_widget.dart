import 'package:bani/config/palette.dart';
import 'package:bani/model/foto_model.dart';
import 'package:bani/widget/photo_viewer_widget.dart';
import 'package:flutter/material.dart';

class FotoWidget extends StatelessWidget {
  Future<FotoModel> futureFotoModel;
  FotoWidget({
    @required this.futureFotoModel,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: FutureBuilder(
        future: futureFotoModel,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            FotoModel fotoModel = snapshot.data;
            return ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
              scrollDirection: Axis.horizontal,
              itemCount: fotoModel.data.length,
              itemBuilder: (BuildContext context, int i) {
                return Card(
                  margin: EdgeInsets.only(
                    right: Pallet.padding / 2,
                    top: 5,
                    bottom: 5,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => PhotoViewerWidget(
                              netwokImage: fotoModel.data[i].foto),
                        ),
                      );
                    },
                    child: Container(
                      width: 100.0,
                      child: ClipRRect(
                        child: FadeInImage.assetNetwork(
                          placeholder: "assets/images/preload_image.png",
                          image: fotoModel.data[i].foto,
                          fit: BoxFit.cover,
                          height: double.infinity,
                          width: double.infinity,
                          alignment: Alignment.center,
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}

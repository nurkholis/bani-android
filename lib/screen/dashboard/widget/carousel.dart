import 'package:bani/config/palette.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/svg.dart';

final List<String> imgList = [
  'assets/svg/banner_1.svg',
  'assets/svg/banner_2.svg',
];

final List<Widget> imageSliders = imgList
    .map((String item) => Container(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: SvgPicture.asset(
                item,
                width: double.infinity,
              ),
            ),
          ),
        ))
    .toList();

class Carousel extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselState();
  }
}

class _CarouselState extends State<Carousel> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          items: imageSliders,
          options: CarouselOptions(
              autoPlay: true,
              enlargeCenterPage: true,
              aspectRatio: 10 / 4,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Pallet.padding,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: imgList.map((url) {
              int index = imgList.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Pallet.primaryColor
                      : Colors.grey[300],
                ),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}

import 'package:bani/config/palette.dart';
import 'package:bani/model/berita_model.dart';
import 'package:bani/screen/berita/isi_berita_screen.dart';
import 'package:flutter/material.dart';

class BeritaWidget extends StatelessWidget {
  Future<BeritaModel> futureBeritaModel;
  BeritaWidget({
    @required this.futureBeritaModel,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 230,
      child: FutureBuilder(
        future: futureBeritaModel,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            BeritaModel beritaModel = snapshot.data;
            return ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: Pallet.padding),
              scrollDirection: Axis.horizontal,
              itemCount: beritaModel.data.length,
              itemBuilder: (BuildContext context, int i) {
                return Padding(
                  padding: EdgeInsets.only(right: Pallet.padding / 2),
                  child: Card(
                    margin: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => IsiBeritaScreen(
                              beritaDataModel: beritaModel.data[i],
                            ),
                          ),
                        );
                      },
                      child: Container(
                        width: 140,
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 140,
                              child: ClipRRect(
                                child: FadeInImage.assetNetwork(
                                  placeholder:
                                      "assets/images/preload_image.png",
                                  image: beritaModel.data[i].cover_berita,
                                ),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text(
                                beritaModel.data[i].judul_berita,
                                maxLines: 4,
                                style: Style.fontSmall,
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}

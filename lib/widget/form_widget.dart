import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:bani/config/palette.dart';

enum ButtonSize { small, medium }

class FormWidget {
  Widget textFormField({
    @required TextEditingController controller,
    @required String label,
    @required TextInputType type,
    int maxLength,
    bool disable,
    IconData icon,
    bool obscureText,
    Function onTap,
    bool idrValue,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15.0),
      child: InkWell(
        onTap: onTap,
        child: TextFormField(
          enabled: onTap == null ? true : false,
          readOnly: disable == null ? false : true,
          keyboardType: type,
          controller: controller,
          obscureText: obscureText == null ? false : true,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(Pallet.borderRadius),
              borderSide: BorderSide(color: Pallet.primaryColor, width: 1.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(Pallet.borderRadius),
              borderSide: BorderSide(color: Colors.black38),
            ),
            labelText: label,
            labelStyle: TextStyle(
              fontSize: Pallet.fontMediumSize,
              color: Colors.black54,
            ),
            prefixIcon: icon != null
                ? Icon(
                    icon,
                    color: Colors.black54,
                    size: Pallet.fontMediumSize,
                  )
                : null,
            suffixIcon: onTap == null
                ? null
                : Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black54,
                    size: Pallet.fontMediumSize,
                  ),
          ),
          maxLength: maxLength,
          inputFormatters: idrValue == null
              ? null
              : [
                  WhitelistingTextInputFormatter.digitsOnly,
                  CurrencyPtBrInputFormatter(),
                ],
          validator: (String value) {
            if (value.isEmpty) {
              return label + ' Harus Diisi';
            }
            return null;
          },
        ),
      ),
    );
  }

  Widget button({
    @required String label,
    @required Function callBack,
    @required bool showLoader,
    Color buttonColor,
    Color textColorColor,
    IconData icon,
    ButtonSize buttonSize,
    double width,
  }) {
    return SizedBox(
      width: buttonSize == null ? double.infinity : width,
      height: buttonSize == null ? 50.0 : 30.0,
      child: RaisedButton(
        onPressed: callBack,
        elevation: 1.0,
        color: buttonColor == null ? Pallet.primaryColor : buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Pallet.borderRadius),
        ),
        child: showLoader
            ? SizedBox(
                width: 16.0,
                height: 16.0,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                  strokeWidth: 2.0,
                ),
              )
            : RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: icon == null
                          ? SizedBox()
                          : Padding(
                              padding: EdgeInsets.only(right: 5),
                              child: Icon(
                                icon,
                                size: buttonSize == ButtonSize.medium ||
                                        buttonSize == null
                                    ? Pallet.fontMediumSize
                                    : Pallet.fontSmallSize,
                                color: textColorColor == null
                                    ? Colors.white
                                    : textColorColor,
                              ),
                            ),
                    ),
                    TextSpan(
                      text: label,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: buttonSize == ButtonSize.medium ||
                                buttonSize == null
                            ? Pallet.fontMediumSize
                            : Pallet.fontSmallSize,
                        color: textColorColor == null
                            ? Colors.white
                            : textColorColor,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}

class CurrencyPtBrInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }
    double value = double.parse(newValue.text);
    String newText =
        NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
            .format(value);
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}

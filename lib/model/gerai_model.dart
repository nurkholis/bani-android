import 'package:bani/model/pagination_model.dart';

class GeraiModel {
  List<GeraiDataModel> data;
  PaginationModel pagination;

  GeraiModel({
    this.data,
    this.pagination,
  });

  factory GeraiModel.fromJson(Map<String, dynamic> json) {
    return GeraiModel(
      data: List<GeraiDataModel>.from(json['data'].map((x) {
        return GeraiDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class GeraiDataModel {
  String id_gerai;
  String id_jenis_gerai;
  String nama_gerai;
  String alamat_gerai;
  String latitude;
  String longitude;
  String nama_pemilik;
  String email;
  String password;
  String foto_gerai;
  String tanggal_terdaftar;
  String akun_premium;
  String status_akun;

  GeraiDataModel({
    this.id_gerai,
    this.id_jenis_gerai,
    this.nama_gerai,
    this.alamat_gerai,
    this.latitude,
    this.longitude,
    this.nama_pemilik,
    this.email,
    this.password,
    this.foto_gerai,
    this.tanggal_terdaftar,
    this.akun_premium,
    this.status_akun,
  });

  factory GeraiDataModel.fromJson(Map<String, dynamic> json) {
    return GeraiDataModel(
      id_gerai: json["id_gerai"].toString(),
      id_jenis_gerai: json["id_jenis_gerai"].toString(),
      nama_gerai: json["nama_gerai"].toString(),
      alamat_gerai: json["alamat_gerai"].toString(),
      latitude: json["latitude"].toString(),
      longitude: json["longitude"].toString(),
      nama_pemilik: json["nama_pemilik"].toString(),
      email: json["email"].toString(),
      password: json["password"].toString(),
      foto_gerai: json["foto_gerai"].toString(),
      tanggal_terdaftar: json["tanggal_terdaftar"].toString(),
      akun_premium: json["akun_premium"].toString(),
      status_akun: json["status_akun"].toString(),
    );
  }
}

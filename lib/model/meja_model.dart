import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class MejaModel {
  List<MejaDataModel> data;
  PaginationModel pagination;

  MejaModel({
    this.data,
    this.pagination,
  });

  factory MejaModel.fromJson(Map<String, dynamic> json) {
    return MejaModel(
      data: List<MejaDataModel>.from(
        json['data'].map((x) {
          return MejaDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class MejaDataModel {
  String id_meja;
  String nama_meja;
  String id_gerai;
  String qrcode_meja;
  GeraiDataModel gerai;

  MejaDataModel({
    this.id_meja,
    this.nama_meja,
    this.id_gerai,
    this.qrcode_meja,
    this.gerai,
  });

  factory MejaDataModel.fromJson(Map<String, dynamic> json) {
    return MejaDataModel(
      id_meja: json["id_meja"].toString(),
      nama_meja: json["nama_meja"].toString(),
      id_gerai: json["id_gerai"].toString(),
      qrcode_meja: json["qrcode_meja"].toString(),
      gerai: json.containsKey("gerai")
          ? GeraiDataModel.fromJson(json['gerai']['data'])
          : null,
    );
  }
}

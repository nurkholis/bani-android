import 'package:bani/model/album_model.dart';
import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class FotoModel {
  List<FotoDataModel> data;
  PaginationModel pagination;

  FotoModel({
    this.data,
    this.pagination,
  });

  factory FotoModel.fromJson(Map<String, dynamic> json) {
    return FotoModel(
      data: List<FotoDataModel>.from(
        json['data'].map((x) {
          return FotoDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class FotoDataModel {
  String id_foto;
  String foto;
  String id_album;
  AlbumDataModel album;

  FotoDataModel({
    this.id_foto,
    this.foto,
    this.id_album,
    this.album,
  });

  factory FotoDataModel.fromJson(Map<String, dynamic> json) {
    return FotoDataModel(
      id_foto: json["id_foto"].toString(),
      foto: json["foto"].toString(),
      id_album: json["id_album"].toString(),
      album: json.containsKey("album")
          ? AlbumDataModel.fromJson(json['album']['data'])
          : null,
    );
  }
}

import 'package:bani/model/pagination_model.dart';

class AlbumModel {
  List<AlbumDataModel> data;
  PaginationModel pagination;

  AlbumModel({
    this.data,
    this.pagination,
  });

  factory AlbumModel.fromJson(Map<String, dynamic> json) {
    return AlbumModel(
      data: List<AlbumDataModel>.from(
        json['data'].map((x) {
          return AlbumDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class AlbumDataModel {
  String id_album;
  String nama_album;
  String cover_album;

  AlbumDataModel({
    this.id_album,
    this.nama_album,
    this.cover_album,
  });

  factory AlbumDataModel.fromJson(Map<String, dynamic> json) {
    return AlbumDataModel(
      id_album: json["id_album"].toString(),
      nama_album: json["nama_album"].toString(),
      cover_album: json["cover_album"].toString(),
    );
  }
}

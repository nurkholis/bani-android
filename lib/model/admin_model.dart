import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class AdminModel {
  List<AdminDataModel> data;
  PaginationModel pagination;

  AdminModel({
    this.data,
    this.pagination,
  });

  factory AdminModel.fromJson(Map<String, dynamic> json) {
    return AdminModel(
      data: List<AdminDataModel>.from(
        json['data'].map((x) {
          return AdminDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class AdminDataModel {
  String id_admin;
  String nama_admin;

  AdminDataModel({
    this.id_admin,
    this.nama_admin,
  });

  factory AdminDataModel.fromJson(Map<String, dynamic> json) {
    return AdminDataModel(
      id_admin: json["id_admin"].toString(),
      nama_admin: json["nama_admin"].toString(),
    );
  }
}

import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class AnggotaModel {
  List<AnggotaDataModel> data;
  PaginationModel pagination;

  AnggotaModel({
    this.data,
    this.pagination,
  });

  factory AnggotaModel.fromJson(Map<String, dynamic> json) {
    return AnggotaModel(
      data: List<AnggotaDataModel>.from(
        json['data'].map((x) {
          return AnggotaDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class AnggotaDataModel {
  String id_anggota;
  String id_bani;
  String id_ibu;
  String id_ayah;
  String email;
  String nama;
  String pasangan;
  String gender;
  String generasi;
  String foto;
  String almarhum;
  AnggotaDataModel ibu;
  AnggotaDataModel ayah;

  AnggotaDataModel({
    this.id_anggota,
    this.id_bani,
    this.id_ibu,
    this.id_ayah,
    this.email,
    this.nama,
    this.pasangan,
    this.gender,
    this.generasi,
    this.foto,
    this.almarhum,
    this.ibu,
    this.ayah,
  });

  factory AnggotaDataModel.fromJson(Map<String, dynamic> json) {
    return AnggotaDataModel(
      id_anggota: json["id_anggota"].toString(),
      id_bani: json["id_bani"].toString(),
      id_ibu: json["id_ibu"].toString(),
      id_ayah: json["id_ayah"].toString(),
      email: json["email"].toString(),
      nama: json["nama"].toString(),
      pasangan: json["pasangan"].toString(),
      gender: json["gender"].toString(),
      generasi: json["generasi"].toString(),
      foto: json["foto"].toString(),
      almarhum: json["almarhum"].toString(),
      ibu: json.containsKey("ibu")
          ? AnggotaDataModel.fromJson(json['ibu']['data'])
          : null,
      ayah: json.containsKey("ayah")
          ? AnggotaDataModel.fromJson(json['ayah']['data'])
          : null,
    );
  }
}

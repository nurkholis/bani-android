class PaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  PaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory PaginationModel.fromJson(Map<String, dynamic> json) {
    return PaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}

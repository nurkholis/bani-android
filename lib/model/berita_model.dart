import 'package:bani/model/admin_model.dart';
import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class BeritaModel {
  List<BeritaDataModel> data;
  PaginationModel pagination;

  BeritaModel({
    this.data,
    this.pagination,
  });

  factory BeritaModel.fromJson(Map<String, dynamic> json) {
    return BeritaModel(
      data: List<BeritaDataModel>.from(
        json['data'].map((x) {
          return BeritaDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class BeritaDataModel {
  String id_berita;
  String judul_berita;
  String cover_berita;
  String tanggal_berita;
  String isi_berita;
  AdminDataModel admin;

  BeritaDataModel({
    this.id_berita,
    this.judul_berita,
    this.cover_berita,
    this.tanggal_berita,
    this.isi_berita,
    this.admin,
  });

  factory BeritaDataModel.fromJson(Map<String, dynamic> json) {
    return BeritaDataModel(
      id_berita: json["id_berita"].toString(),
      judul_berita: json["judul_berita"].toString(),
      cover_berita: json["cover_berita"].toString(),
      tanggal_berita: json["tanggal_berita"].toString(),
      isi_berita: json["isi_berita"].toString(),
      admin: json.containsKey("admin")
          ? AdminDataModel.fromJson(json['admin']['data'])
          : null,
    );
  }
}

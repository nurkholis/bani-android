import 'package:bani/model/gerai_model.dart';
import 'package:bani/model/pagination_model.dart';

class LaporanModel {
  List<LaporanDataModel> data;
  PaginationModel pagination;

  LaporanModel({
    this.data,
    this.pagination,
  });

  factory LaporanModel.fromJson(Map<String, dynamic> json) {
    return LaporanModel(
      data: List<LaporanDataModel>.from(
        json['data'].map((x) {
          return LaporanDataModel.fromJson(x);
        }),
      ),
      pagination: json.containsKey("meta")
          ? PaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class LaporanDataModel {
  String id_laporan;
  String keterangan_laporan;
  String laporan;

  LaporanDataModel({
    this.id_laporan,
    this.keterangan_laporan,
    this.laporan,
  });

  factory LaporanDataModel.fromJson(Map<String, dynamic> json) {
    return LaporanDataModel(
      id_laporan: json["id_laporan"].toString(),
      keterangan_laporan: json["keterangan_laporan"].toString(),
      laporan: json["laporan"].toString(),
    );
  }
}
